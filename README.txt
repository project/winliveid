
Windows Live ID

Authors: Anon <panis @ drupal forums>

Links
-----
Please consult the Windows Live ID Web Authentication SDK documentation 
for the PHP QuickStart Sample instructions:

http://go.microsoft.com/fwlink/?LinkID=91762

Before you Install/Use
----------------------
1. mcrypt and mhash  extensions for PHP - required to decrypt information sent and received by the windows live service.
2. clean URLS - windows live service needs a redirection url to jump to on the drupal installation. this URL cannot contain query arguments (& and ?). If you are not able to install clean URL there are a couple of workarounds:
 a) rewrite rules in your .htaccess file for just the callback url
 b) use the webauth-handler.php file as a proxy and stick it in your document
   directory with appropriate modifications.

Install
-------
1. Copy to your modules directory 
2. Register and get your key from microsoft - You will need a couple of things
   before you can get your key. 

       a) Your own windows live account 
        - create one right then or use your hotmail/msn account if you have one.
 	   b) A secret pass code - you will be prompted for this and will need to
          update the XML file on your server with this pass code.

       c) The callback URL for the windows live service that handles the login 
          and logout calls.  This will be 
		    "http://your_site_name/drupal_suffix_if_any/winliveid/handler"
          replace with your sitename and any drupal path suffix if present.

		  If you do not have CLEAN URLS and cannot have CLEAN URLS - use the
          included webauth-handler.php file as a proxy and stick it in your
          document root and your callback URL then will be:
		    "http://your_site_name/path_to_webauth_handler.php"
          Remember to modify the webauth-handler file.
          
OR
3. If you want to play around without registering setup the 
   webauth-handler.php file as per the instructions in it.
   This will let you use the sample windows id key and has a workaround for you
   to use without getting your key.

4. If you have your key and passcode edit the ApplicationKey.xml file and enter
   them there per instruction from the windows live.

   If you are testing the sample - leave the xml file as is.

5. Enable the module and you should be ready to go. You should see the "Windows Live ID" Sign In link on the login block.

6. For a new user to the site - if they choose to login via Windows Live
   It will prompt them to create a new user on your site and go through the 
   steps you have setup for registering a new user.

   Existing users on your site can setup to login via windows live by:
    - going to their "account page" and clicking on the "Windows Live ID" tab
    - Sign on to Windows Live from the link provided
    - and they should be set to go.

Bugs
-------
1. the module does not yet remember state - ie where you were when you logged in through windows live - it will always dump you back to the default home page. The windows live authentication mechanism is closed and a bit quirky. I am trying to figure out the best way to fix this.
