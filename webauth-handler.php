<?php 

/* dummy webauth-handler for Windows Live ID testing. 
 * The test sample for Windows Live ID when used without your own 
 * provider id  always forwards to the following location: 
 * http://your.host.name/webauth/sample/webauth-handler.php
 *
 * You can work around that in two ways during testing with your drupal 
 * installation:
 *
 * 1) use a rewriteurl in your httpd.conf or .htaccess file 
 *    add the following line or lines 
 *    <Directory path_to_your_document_root/webauth/sample>
 *    RewriteEngine on
 *    RewriteRule ^.*webauth-handler.php(.*)$ /path_to_drupal_base/?q=winliveid/handler&$1 [L,QSA]
 *    </Directory>
 *
 * 2) Your mileage may vary here.
 *    copy this webauth-handler.php file under 
 *     path_to_your_document_root/webauth/sample/
 *
 *     edit the file to point to your drupal installation and this will forward
 *     all calls over to the appropriate drupal handler path.
 */

/**
 * EDIT to your drupal url path. Defaults to root
 **/
$drupal_path = "/";

/**
 * Get everything off the Request and forward to drupal.
 * Currently limited to the following:
 * action
 * stoken
 * appctx
 **/

$q = 'action='. urlencode($_REQUEST['action']) 
	. '&stoken='. urlencode($_REQUEST['stoken']) 
	. '&appctx=' . urlencode($_REQUEST['appctx']);

header('Location: ' . $drupal_path . '?q=winliveid/handler&' . $q);
?>
